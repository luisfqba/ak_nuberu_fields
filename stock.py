# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, orm, fields
from openerp import netsvc
import time
import datetime

from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

# ----------------------------------------------------
# Incoveniente al cambiar la Categoria del Producto debido a que
# en stock_move se almacena tiene que guardar la Categoria
# implementar luego funcionalidad para actualizar la Categoria almacenada
# ----------------------------------------------------

class stock_move(orm.Model):
    _inherit = "stock.move"

    def _get_invoice_state(self, cr, uid, ids, field_name, arg, context=None):
        records = self.browse(cr, uid, ids)
        res = {}
        for r in records:
            if r.type in ['out','in']:
                res[r.id] = r.picking_id.invoice_state
        return res

    _columns = {
        'categ_id': fields.related('product_id','categ_id','name',type='char', relation="product.product", string="Categoria Producto", store=True),
        'invoice_state': fields.function(_get_invoice_state, type='char',
                                      string='Estado de Factura', store=True),
        'return_type': fields.selection([
            ('1', 'Defecto (Calidad)'),
        ], 'Motivo Devolucion', select=True, ),
    }

class stock_picking(orm.Model):
    _inherit = 'stock.picking'

    def _get_string_date(self, cr, uid, ids, name, args, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):
            res[record.id] = str(record.date)
        return res

    _columns = {
        'client_order_ref': fields.related('sale_id','client_order_ref',type='char', relation='sale.order',
                                   string='Refencia del Cliente'),
        'create_date': fields.datetime('Fecha de Creacion', readonly=True),
        'return_type': fields.selection([
            ('1', 'Defecto (Calidad)'),
        ], 'Motivo Devolucion', select=True, ),
        'x_string_date': fields.function(_get_string_date, type='char', string='Date (String)', store=True),
    }

    def create(self, cr, uid, vals, context=None):
        res = super(stock_picking, self).create(cr, uid, vals, context=context)
        if vals.get('type') == 'out':
            document = self.browse(cr, uid, res, context=context)
            # obteniendo el grupo de almacen
            dummy_stock, group_id_stock = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock',
                                                                                              'group_stock_user')
            # obteniendo los usuarios
            users_obj = self.pool.get('res.users')
            users_id = users_obj.search(cr, uid, [('active', '=', True)])
            users_list_obj = self.pool.get('res.users').browse(cr, uid, users_id, context)

            for u in users_list_obj:
                id_tmp = u.groups_id[0].id
                user_group_ids = [user_group_id.id for user_group_id in u.groups_id]
                if group_id_stock in user_group_ids:
                    if u.partner_id.id not in document.message_follower_ids:
                        self.message_subscribe(cr, uid, [res], [u.partner_id.id], context=context)
        return res

    def action_done(self, cr, uid, ids, context=None):
        res = super(stock_picking, self).action_done(cr, uid, ids, context=context)
        document = self.browse(cr, uid, ids, context=context)
        if res and document[0].type == 'out':
            records = self._get_followers(cr, uid, ids, None, None, context=context)
            followers = records[ids[0]]['message_follower_ids']
            self.message_post(cr, uid, ids, body="Entregado %s" % (document[0].name),
                              subtype='mt_comment',
                              partner_ids=followers,
                              context=context)
        return res

class stock_picking_out(orm.Model):
    _inherit = 'stock.picking.out'

    def _get_string_date(self, cr, uid, ids, name, args, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):
            res[record.id] = str(record.date)
        return res

    _columns = {
        'client_order_ref': fields.related('sale_id','client_order_ref',type='char', relation='sale.order',
                                   string='Refencia Cliente'),
        'create_date': fields.datetime('Fecha de Creacion', readonly=True),
        'return_type': fields.selection([
            ('1', 'Defecto (Calidad)'),
        ], 'Motivo Devolucion', select=True, ),
        'x_string_date': fields.function(_get_string_date, type='char', string='Date (String)', store=True),
    }

    def create(self, cr, uid, vals, context=None):
        res = super(stock_picking_out, self).create(cr, uid, vals, context=context)
        document = self.browse(cr, uid, res, context=context)
        # obteniendo el grupo de almacen
        dummy_stock, group_id_stock = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock',
                                                                                          'group_stock_user')
        # obteniendo los usuarios
        users_obj = self.pool.get('res.users')
        users_id = users_obj.search(cr, uid, [('active', '=', True)])
        users_list_obj = self.pool.get('res.users').browse(cr, uid, users_id, context)

        for u in users_list_obj:
            id_tmp = u.groups_id[0].id
            user_group_ids = [user_group_id.id for user_group_id in u.groups_id]
            if group_id_stock in user_group_ids:
                if u.partner_id.id not in document.message_follower_ids:
                    self.message_subscribe(cr, uid, [res], [u.partner_id.id], context=context)
        return res

class stock_picking_in(orm.Model):
    _inherit = 'stock.picking.in'

    _columns = {
        'create_date': fields.datetime('Fecha de Creacion', readonly=True),
    }

class stock_return_picking(osv.osv_memory):
    # wizard para retornar productos, se le agrega motivo de devolucion para poder evaluar las devoluciones.
    _inherit = 'stock.return.picking'
    _columns = {
        'return_type' : fields.selection([
            ('1','Defecto (Calidad)'),
            ],'Motivo Devolucion', select=True, ),
    }

    def create_returns(self, cr, uid, ids, context=None):
        """
         Creates return picking.
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param ids: List of ids selected
         @param context: A standard dictionary
         @return: A dictionary which of fields with values.
        """
        if context is None:
            context = {}
        record_id = context and context.get('active_id', False) or False
        move_obj = self.pool.get('stock.move')
        pick_obj = self.pool.get('stock.picking')
        uom_obj = self.pool.get('product.uom')
        data_obj = self.pool.get('stock.return.picking.memory')
        act_obj = self.pool.get('ir.actions.act_window')
        model_obj = self.pool.get('ir.model.data')
        wf_service = netsvc.LocalService("workflow")
        pick = pick_obj.browse(cr, uid, record_id, context=context)
        data = self.read(cr, uid, ids[0], context=context)
        date_cur = time.strftime('%Y-%m-%d %H:%M:%S')
        set_invoice_state_to_none = True
        returned_lines = 0

        #        Create new picking for returned products

        seq_obj_name = 'stock.picking'
        new_type = 'internal'
        if pick.type == 'out':
            new_type = 'in'
            seq_obj_name = 'stock.picking.in'
        elif pick.type == 'in':
            new_type = 'out'
            seq_obj_name = 'stock.picking.out'
        new_pick_name = self.pool.get('ir.sequence').get(cr, uid, seq_obj_name)
        new_picking = pick_obj.copy(cr, uid, pick.id, {
            'name': _('%s-%s-return') % (new_pick_name, pick.name),
            'move_lines': [],
            'state': 'draft',
            'backorder_id': False,
            'type': new_type,
            'date': date_cur,
            'invoice_state': data['invoice_state'],
        })

        val_id = data['product_return_moves']
        for v in val_id:
            data_get = data_obj.browse(cr, uid, v, context=context)
            # start luisfqba
            return_type = data_get.wizard_id.return_type
            # end luisfqba
            mov_id = data_get.move_id.id
            if not mov_id:
                raise osv.except_osv(_('Warning !'),
                                     _("You have manually created product lines, please delete them to proceed"))
            new_qty = data_get.quantity
            move = move_obj.browse(cr, uid, mov_id, context=context)
            new_location = move.location_dest_id.id
            returned_qty = move.product_qty
            for rec in move.move_history_ids2:
                returned_qty -= rec.product_qty

            if returned_qty != new_qty:
                set_invoice_state_to_none = False
            if new_qty:
                returned_lines += 1
                new_move = move_obj.copy(cr, uid, move.id, {
                    'product_qty': new_qty,
                    'product_uos_qty': uom_obj._compute_qty(cr, uid, move.product_uom.id, new_qty, move.product_uos.id),
                    'picking_id': new_picking,
                    'state': 'draft',
                    'location_id': new_location,
                    'location_dest_id': move.location_id.id,
                    'date': date_cur,
                    'prodlot_id': data_get.prodlot_id.id,
                    'return_type': return_type,
                })
                move_obj.write(cr, uid, [move.id], {'move_history_ids2': [(4, new_move)]}, context=context)
        if not returned_lines:
            raise osv.except_osv(_('Warning!'), _("Please specify at least one non-zero quantity."))

        if set_invoice_state_to_none:
            pick_obj.write(cr, uid, [pick.id], {'invoice_state': 'none'}, context=context)
        wf_service.trg_validate(uid, 'stock.picking', new_picking, 'button_confirm', cr)
        pick_obj.force_assign(cr, uid, [new_picking], context)
        # Update view id in context, lp:702939
        model_list = {
            'out': 'stock.picking.out',
            'in': 'stock.picking.in',
            'internal': 'stock.picking',
        }
        return {
            'domain': "[('id', 'in', [" + str(new_picking) + "])]",
            'name': _('Returned Picking'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': model_list.get(new_type, 'stock.picking'),
            'type': 'ir.actions.act_window',
            'context': context,
        }


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
