# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.osv.orm import setup_modifiers
import datetime
import math

class res_partner(osv.Model):
    _inherit = 'res.partner'
    """
        def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        res = osv.Model.fields_view_get(self, cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)
        #Verifico que tenga permisos
        dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'base', 'group_erp_manager')
        #user = self.pool.get('res.users').browse(cr, uid, uid)
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
        can_create = False
        if group_id in user_group_ids:
            can_create = True



        #default_type = context.get('default_type', False)
        #can_create = default_type != 'out_customer_production'
        update = not can_create and view_type in ['form', 'tree','search','kanban']
        if update:
            from lxml import etree
            doc = etree.XML(res['arch'])
            if not can_create:
                for t in doc.xpath("//"+view_type):
                    t.attrib['create'] = 'false'
                    #t.attrib['edit'] = 'false'
            res['arch'] = etree.tostring(doc)

        return res
    """

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        res = super(res_partner, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type,
                                                                 context=context, toolbar=toolbar, submenu=submenu)
        type = context.get('default_type', False)



        default_type = context.get('default_type', False)
        #can_create = default_type != 'out_customer_production'
        update = view_type in ['form']
        if update:
            from lxml import etree
            doc = etree.XML(res['arch'])
            #Numero Externo
            for node in doc.xpath("//field[@name='l10n_mx_street3']"):
                #node.set('readonly', 'True')
                node.set("required", "True")
                #node.attrib['readonly'] = '1'
                setup_modifiers(node, res['fields']['l10n_mx_street3'])
            #Direccion
            for node in doc.xpath("//field[@name='street']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['street'])
            #Numero Interno
            for node in doc.xpath("//field[@name='l10n_mx_street4']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['l10n_mx_street4'])
            #Colonia
            for node in doc.xpath("//field[@name='street2']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['street2'])

            #Ciudad
            for node in doc.xpath("//field[@name='city_id']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['city_id'])

            #Estado
            for node in doc.xpath("//field[@name='state_id']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['state_id'])
            #Zip
            for node in doc.xpath("//field[@name='zip']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['zip'])

            #Localidad
            for node in doc.xpath("//field[@name='l10n_mx_city2']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['l10n_mx_city2'])

            #Pais
            for node in doc.xpath("//field[@name='country_id']"):
                node.set("required", "True")
                setup_modifiers(node, res['fields']['country_id'])

            res['arch'] = etree.tostring(doc)
    
        return res

    def _is_manager(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids: return res
        result = False

        dummy, group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'base', 'group_erp_manager')
        user_group_ids = self.pool.get('res.users').browse(cr, uid, [uid], context)[0].groups_id
        for record in self.browse(cr, uid, ids, context=context):
            for g_id in user_group_ids:
                if group_id == g_id.id:
                    result = True
                    res[record.id] = result
                    return res
        res[record.id] = result
        return res

    def _calc_eval_servicio(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids: return res
        result = 0.0
        for record in self.browse(cr, uid, ids, context=context):
            result = float(record.eval_precio) + float(record.eval_tiempo_entrega) + float(record.eval_calidad_producto) + float(record.eval_condiciones_pago)
            res[record.id] = result/4.0
        return res

    def _calc_eval_promedio(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids: return res
        result = 0.0
        for record in self.browse(cr, uid, ids, context=context):
            result = float(record.eval_precio) + float(record.eval_tiempo_entrega) + float(
                record.eval_calidad_producto) + float(record.eval_condiciones_pago) + float(record.eval_servicio)
            res[record.id] = result / 5.0
        return res

    def _calc_eval_precio(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids: return res
        result = 0

        record = self.browse(cr, uid, ids, context=context)[0]
        fecha = datetime.datetime.now()
        semestre =1
        if fecha.month > 6:
            semestre = 2
        year = fecha.year
        query_sql_semestre1 = (
            "select AVG(cal) \n"
            "from clasificacion_proveedor_precio \n"
            "where sem=%s and year='%s' and partner_id =%s \n") % (semestre,str(year), record.id)
        cr.execute(query_sql_semestre1)
        promedio = cr.fetchone()[0]
        if not promedio:
            promedio = 0.00
        frac, entero = math.modf(promedio)
        res[record.id] = entero
        return res

    def _calc_eval_tiempo_entrega(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids: return res
        result = 0

        record = self.browse(cr, uid, ids, context=context)[0]
        fecha = datetime.datetime.now()
        semestre = 1
        if fecha.month > 6:
            semestre = 2
        year = fecha.year
        query_sql = ("select cal \n"
                     "from clasificacion_proveedor_tiempo \n"
                     "where sem=%s and year='%s' and partner_id =%s \n") % (semestre, str(year), record.id)
        cr.execute(query_sql)

        calificacion = cr.fetchone()
        if calificacion:
            result = calificacion[0]
        res[record.id] = result
        return res

    def _calc_eval_calidad(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids: return res
        result = 0

        record = self.browse(cr, uid, ids, context=context)[0]
        fecha = datetime.datetime.now()
        semestre = 1
        if fecha.month > 6:
            semestre = 2
        year = fecha.year
        query_sql = ("select cal \n"
                     "from clasificacion_proveedor_calidad \n"
                     "where sem=%s and year='%s' and partner_id =%s \n") % (semestre, str(year), record.id)
        print query_sql
        cr.execute(query_sql)
        calificacion = cr.fetchone()
        if calificacion:
            result = calificacion[0]
        res[record.id] = result
        return res

    _columns = {
        'manager': fields.function(_is_manager, type='boolean', method=True, string='Manager'),
        'eval_precio': fields.integer('Precio'),
        'eval_tiempo_entrega':fields.integer('Tiempo Entrega'),
        'eval_calidad_producto':fields.integer('Calidad'),
        'eval_condiciones_pago':fields.selection([
            ('4','4 para mas de 30 dias de credito'),
            ('3','3 para creditos de entre 15 dias y 30 dias'),
            ('2','2 para creditos de 1 a 14 dias'),
            ('1','1 para proveedores que exigen anticipo'),
            ],'Condiciones Pago', select=True, ),
        'eval_servicio': fields.integer(string='Servicio'),
        'eval_promedio': fields.function(_calc_eval_promedio, type='float', method=True, string='Promedio'),
        'clasificacion': fields.selection([
            ('A', 'A: Materia prima, Herramienta, Insertos, Maquinaria, entre otros.'),
            ('B', 'B: Servicios de limpieza, cursos de capacitacion, entre otros.'),
            ('C', 'C: Servicios de mensajeria, entre otros.'),
            ], 'Clasificacion', select=True, ),
        'products_comments': fields.text('Nota Productos/Servicios'),
    }
    def create(self, cr, uid, vals, context=None):
        #Tipo de Direccion
        type_in = vals.get('type')
        if not type_in:
            raise osv.except_osv('Warning!',"Tiene que asignar Tipo de Direccion.")
        
        #Contacto
        ## Si es una empresa se tiene que ingresar al menos un contacto
        """is_company_in = vals.get('is_company')
        if is_company_in:
            contact_in = vals.get('child_ids')
            if not contact_in:
                raise osv.except_osv('Warning!',"Tiene que asignar al menos un Contacto.")
        """
        #Validar que no se capture dos veces el mismos Nombre para evitar duplicidad
        name_in = vals.get('name')
        res_partner_exit_name=self.pool.get('res.partner').search(cr, uid, [('name','ilike',name_in)])
        
        
        if res_partner_exit_name:
            raise osv.except_osv('Warning!',"Ya existe un Cliente/Proveedor con el Nombre %s debe asignar otro."%(name_in))
        
        #RFC
        """
        rfc = vals.get('vat')
        ##verificar que se ingrese el RFC
        if not rfc:
            raise osv.except_osv('Warning!',"Tiene que ingresar un RFC"%(rfc))
        """
        ##verificar si el RFC existe y es distinto del RFC genérico...
        rfc_in = vals.get('vat')
        if rfc_in and rfc_in != "MXXEXX010101000":
            res_partner_exit_id=self.pool.get('res.partner').search(cr, uid, [('vat','ilike',rfc_in)])
            if res_partner_exit_id and rfc_in:
                raise osv.except_osv('Warning!',"Ya existe un Cliente/Proveedor con el RFC %s debe asignar otro RFC."%(rfc_in))

        #Regimen Fiscal
        """
        if not vals.get('regimen_fiscal_id'):
            raise osv.except_osv('Warning!',"Tiene que ingresar el Regimen Fiscal") 
        """
        #Banco
        #if not vals.get('bank_ids'):
        #        raise osv.except_osv('Warning!',"Tiene ingresar al menos un Banco.")
        
        #Validaciones Proveedores        
        """if vals.get('supplier'):
            ##Tarifa de compra Proveedores
            if not vals.get('property_product_pricelist_purchase'):
                raise osv.except_osv('Warning!',"Ingrese la Tarifa de Compra al Proveedor.")
            #Plazo de Pago del Proveedores
            if not vals.get('property_supplier_payment_term'):
                raise osv.except_osv('Warning!',"Ingrese el Plazo de pago del proveedor.")
            #Tipode Pago del Proveedores
            if not vals.get('payment_type_supplier'):
                raise osv.except_osv('Warning!',"Ingrese Tipo de pago del proveedor.")
       """ 
        #Validaciones Clientes
        if vals.get('customer'):
            ##solo pude crear Clientes el gerente de Ventas
            dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'base', 'group_sale_manager')
            user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
            if group_id in user_group_ids:
                pass
            else:
                raise osv.except_osv('Warning!',"Solo los Gerentes de Ventas pueden crear Clientes.")
            ##Validar toda la direccion
            ##Tarifa de compra Proveedores
            """if not vals.get('property_product_pricelist'):
                raise osv.except_osv('Warning!',"Ingrese la Tarifa de Compra al Proveedor.")
            #Plazo de Pago del Cliente
            if not vals.get('property_supplier_payment_term'):
                raise osv.except_osv('Warning!',"Ingrese el Plazo de pago del proveedor.")
            #Tipode Pago del Cliente
            if not vals.get('payment_type_customer'):
                raise osv.except_osv('Warning!',"Ingrese Tipo de pago del proveedor.")
            """
        
        return super(res_partner, self).create(cr, uid, vals, context = context)
    
    def write(self, cr, uid, ids, vals, context=None):
        #obtengo datos del partner
        try:
            res_partner_obj=self.pool.get('res.partner').browse(cr, uid, ids, context)[0]
        except:
            return super(res_partner, self).write(cr, uid, ids, vals, context = context)
        #Tipo de Direccion
        type_in = res_partner_obj.type
        if not type_in:
            raise osv.except_osv('Warning!',"Tiene que asignar Tipo de Direccion.")
       
        #Validar que no se capture dos veces el mismos Nombre para evitar duplicidad
        name_in = vals.get('name')
        if name_in:            
            res_partner_exit_name=self.pool.get('res.partner').search(cr, uid, [('name','=',name_in)])
            if res_partner_exit_name:
                raise osv.except_osv('Warning!',"Ya existe un Cliente/Proveedor con el Nombre %s debe asignar otro."%(name_in))
        
        #RFC
        try:
            rfc_in = vals.get('vat')            
            ##verificar si el RFC existe
            if rfc_in and rfc_in != "MXXEXX010101000":
                rfc = res_partner_obj.vat              
                res_partner_exit_ids=self.pool.get('res.partner').search(cr, uid, [('vat','=',rfc_in)])
                        
                if res_partner_exit_ids and rfc_in:
                    raise osv.except_osv('Warning!',"Ya existe un Cliente/Proveedor con el RFC %s debe asignar otro RFC."%(rfc))
        except:
            pass    

        #Validaciones Clientes
        if res_partner_obj.customer or vals.get('customer'):
            ##solo pude crear Clientes el gerente de Ventas
            dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'base', 'group_sale_manager')
            user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
            if group_id in user_group_ids:
                pass
            else:
                raise osv.except_osv('Warning!',"Solo los Gerentes de Ventas pueden crear Clientes.")

        return super(res_partner, self).write(cr, uid, ids, vals, context = context)

    def on_change_load_eval_condiciones_pago(self, cr, uid, ids, property_supplier_payment_term, context=None):
        values = {'eval_condiciones_pago': None}
        if property_supplier_payment_term:
            if property_supplier_payment_term in [12, 10, 11, 4]:
                values['eval_condiciones_pago'] = '4'
            elif property_supplier_payment_term in [2, 3]:
                values['eval_condiciones_pago'] = '3'
            elif property_supplier_payment_term in [5, 9]:
                values['eval_condiciones_pago'] = '2'
            elif property_supplier_payment_term in [6, 8, 13, 7, 1]:
                values['eval_condiciones_pago'] = '1'

        return {'value': values}
