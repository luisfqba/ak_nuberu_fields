# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Copyright (c) 2012 Vauxoo - http://www.vauxoo.com
#    All Rights Reserved.
#    info@vauxoo.com
############################################################################
#    Coded by: Juan Carlos Funes (juan@vauxoo.com)
#    Coded by: Luis Torres (luis_t@vauxoo.com)
#    Coded by: moylop260 (moylop260@vauxoo.com)
#    Coded by: isaac (isaac@vauxoo.com)
############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields
from openerp.osv.orm import setup_modifiers
import re

class res_partner_bank(osv.Model):
    _inherit = 'res.partner.bank'

    _columns = {
        'sucursal': fields.char("Sucursal"),
    }

    def _convertir_patron_nuberu_clabe(self, clave):
        nueva_clave = re.sub(r"(\d{3})", r'\1-', clave)
        if nueva_clave[len(nueva_clave) - 1] == '-':
            nueva_clave = nueva_clave.rstrip("-")
        return nueva_clave

    def _check_clabe_acc_number(self, clave):
        # se valida que solo se ingresen digitos
        patron_clabe = re.compile(
            '\d+'
        )
        match_clabe = patron_clabe.match(clave)
        if not match_clabe:
            return False
        return True


    def create(self, cr, uid, vals, context=None):
        clabe_in = vals.get('clabe')
        if clabe_in:
            if self._check_clabe_acc_number(clabe_in):
                vals['clabe'] = self._convertir_patron_nuberu_clabe(clabe_in)
            else:
                raise osv.except_osv('Warning!',
                                     "Clabe de la Cuenta de Bancaria invalida.\n Solamente puede ingresar digitos")

        acc_number_in = vals.get('acc_number')
        if acc_number_in:
            if self._check_clabe_acc_number(acc_number_in):
                vals['acc_number'] = self._convertir_patron_nuberu_clabe(acc_number_in)
            else:
                raise osv.except_osv('Warning!',
                                     "Numero de cuenta Bancaria invalido.\n Solamente puede ingresar digitos")

        return super(res_partner_bank, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        clabe_in = vals.get('clabe')
        if clabe_in:
            if self._check_clabe_acc_number(clabe_in):
                vals['clabe'] = self._convertir_patron_nuberu_clabe(clabe_in)
            else:
                raise osv.except_osv('Warning!',
                                     "Clabe de la Cuenta de Bancaria invalida.\n Solamente puede ingresar digitos")

        acc_number_in = vals.get('acc_number')
        if acc_number_in:
            if self._check_clabe_acc_number(acc_number_in):
                vals['acc_number'] = self._convertir_patron_nuberu_clabe(acc_number_in)
            else:
                raise osv.except_osv('Warning!', "Numero de cuenta Bancaria invalido.\n Solamente puede ingresar digitos")

        return super(res_partner_bank, self).write(cr, uid, ids, vals, context=context)
