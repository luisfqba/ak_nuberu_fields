# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Ak Nuberu Campos',
    'version': '0.1',
    'description': """Modulo que agrega nuevos campos a Modelos Nuberu""",
    'author': 'luisfqba@gmail.com',
    'update_xml': [
        "mrp_view.xml","sale_view.xml","invoice_view.xml","stock_view.xml","purchase_view.xml","product_view.xml","account_voucher_view.xml","res_partner.xml","bank_view.xml"
        ],
    'data': ['security/ir.model.access.csv','security/account_security.xml','data/cron_data.xml'],
    "depends": [
            "base","mrp","purchase","purchase_requisition","account_payment_type","l10n_mx_regimen_fiscal","l10n_mx_res_partner_bank"
                ],
    'active': True,
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
