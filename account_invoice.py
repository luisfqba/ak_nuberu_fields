from openerp.osv import osv, fields

class account_invoice(osv.Model):
    _inherit = 'account.invoice'

    _columns = {
        'create_date': fields.datetime('Fecha de Creacion', readonly=True),
    }

    def create(self, cr, uid, vals, context=None):
        #account_invoice_obj=account_invoice_obj.browse(cr, uid, ids[0], context=context)       
        if context.get('type') == "in_invoice" or context.get('inv_type') == "in_invoice":
            # Perform your checks on user groups here
            ## If the person is not a sales manager, show them an error message
            ##dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'purchase', 'group_purchase_manager')
            #dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'base', 'group_erp_manager')
            user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
            #usuario restriccion en compras
            dummy_compra_rest,group_id_compra_rest = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_purchase_restriction')
            #usuarios de almacen
            #dummy_stock,group_id_stock = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'group_stock_user')
            #usuarios de contabilidad
            #dummy_account,group_id_account = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'group_account_user')        
            #if group_id_stock not in user_group_ids or group_id_account not in user_group_ids:
            if group_id_compra_rest in user_group_ids:
                
            #all_groups=self.pool.get('res.groups')
            #edit_group = all_groups.browse(cr, uid, all_groups.search(cr,uid,[('name','=','Producto solo lectura')])[0])
            #groups_users=edit_group.users
            #for groups_user in groups_users:
                #if uid == groups_user.id:
                raise osv.except_osv('Warning!',"Usted no tiene permiso para Crear Facturas de Proveedor!")
            else:
                return super(account_invoice, self).create(cr, uid,  vals, context = context)
        return super(account_invoice, self).create(cr, uid, vals, context = context)

    def write(self, cr, uid, ids, vals, context=None):
        if context:
            rest = False
            if 'type' in context:
                if context['type'] == "in_invoice":
                    rest = True
            else:
                if 'inv_type' in context:
                    if context['inv_type'] == "in_invoice":
                        rest = True
            if rest:
                user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
                #usuario restriccion en compras
                dummy_compra_rest,group_id_compra_rest = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_purchase_restriction')
                #usuarios de almacen
                #dummy_stock,group_id_stock = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'group_stock_user')
                #usuarios de contabilidad
                #dummy_account,group_id_account = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'group_account_user')
                #if group_id_stock not in user_group_ids or group_id_account not in user_group_ids:
                if group_id_compra_rest in user_group_ids:
                    raise osv.except_osv('Warning!',"Usted no tiene permiso para Modificar/Crear Facturas de Proveedor!")
                else:
                    return super(account_invoice, self).write(cr, uid, ids, vals, context = context)
        return super(account_invoice, self).write(cr, uid, ids, vals, context = context)

    def unlink(self, cr, uid, ids, context=None):
        try:
            id_invoice = ids[0] 
        except:
            id_invoice = ids
        account_invoice_obj=self.pool.get('account.invoice').browse(cr, uid, id_invoice, context=context)       
        if account_invoice_obj.type == "in_invoice":
            user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
            #usuario restriccion en compras
            dummy_compra_rest,group_id_compra_rest = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_purchase_restriction')
            #usuarios de almacen
            #dummy_stock,group_id_stock = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'group_stock_user')
            #usuarios de contabilidad
            #dummy_account,group_id_account = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'group_account_user')
            #if group_id_stock not in user_group_ids or group_id_account not in user_group_ids:   
            if group_id_compra_rest in user_group_ids:
                raise osv.except_osv('Warning!',"Usted no tiene permiso para Eliminar Facturas de Proveedor!")
            else:
                return super(account_invoice, self).unlink(cr, uid, ids, context = context)        
        return super(account_invoice, self).unlink(cr, uid, ids, context = context)
    
    def copy(self, cr, uid, ids, default=None, context=None):
        try:
            id_invoice = ids[0] 
        except:
            id_invoice = ids
        account_invoice_obj=self.pool.get('account.invoice').browse(cr, uid, id_invoice, context=context)
        if account_invoice_obj.type == "in_invoice":
            user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
            #usuario restriccion en compras
            dummy_compra_rest,group_id_compra_rest = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_purchase_restriction')
            #usuarios de almacen
            #dummy_stock,group_id_stock = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'group_stock_user')
            #usuarios de contabilidad
            #dummy_account,group_id_account = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'group_account_user')
            #if group_id_stock not in user_group_ids or group_id_account not in user_group_ids:   
            if group_id_compra_rest in user_group_ids:
                raise osv.except_osv('Warning!',"Usted no tiene permiso para Duplicar  Facturas de Proveedor!")
            else:
                return super(account_invoice, self).copy(cr, uid, ids, default=default, context = context)        
        return super(account_invoice, self).copy(cr, uid, ids, default=default, context = context)
