from openerp.osv import osv, fields

class product_product(osv.Model):
    _inherit = 'product.product'

    def get_rate_price_supllier(self, cr, uid, ids, name, args, context=None):
        """Funcion que muestra el precio unitario del primer proveedor del producto"""
        res = {}
        if not ids: return res
        result = 0.0
        for record in self.browse(cr, uid, ids, context=context):
            try:
                result = record.seller_ids[0].pricelist_ids[0]['price']
            except:
                pass
            res[record.id] = result
        return res
    
    def get_group_purchase_ok(self, cr, uid, ids, name, args, context=None):
        """Funcion que devuelve"""
        res = {}
        if not ids: return res
        result = False
        """all_groups=self.pool.get('res.groups')
        edit_group = all_groups.browse(cr, uid, all_groups.search(cr,uid,[('name','=','Producto solo lectura')])[0])
        groups_users=edit_group.users
        """
        # Perform your checks on user groups here
        ## If the person is not a sales manager, show them an error message
        #dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'purchase', 'group_purchase_user')
        dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_purchase_restriction')
        #user = self.pool.get('res.users').browse(cr, uid, uid)
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
        for record in self.browse(cr, uid, ids, context=context):
            if group_id in user_group_ids and uid != 1:
                result = True
                res[record.id] = result
                return res
        res[record.id] = result
        return res
        """for record in self.browse(cr, uid, ids, context=context):
            for groups_user in groups_users:
                if uid == groups_user.id:
                    result = True
                    res[record.id] = result
                    return res
        res[record.id] = result
        return res
        """
    def get_group_sale_ok(self, cr, uid, ids, name, args, context=None):
        """Funcion que devuelve"""
        res = {}
        if not ids: return res
        result = False
        
        # Perform your checks on user groups here
        ## If the person is not a sales manager, show them an error message
        dummy,group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'base', 'group_sale_salesman')
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
        for record in self.browse(cr, uid, ids, context=context):
            #if group_id in user_group_ids and uid != 1:
            if group_id in user_group_ids:
                result = True
                res[record.id] = result
                return res
        res[record.id] = result
        return res
    
    _columns = {
        'rate_price_supllier': fields.function(get_rate_price_supllier, type='float', method=True, string='Precio Unitario Proveedor'),
        'group_purchase_ok': fields.function(get_group_purchase_ok, type='boolean', method=True, string='group_purchase_ok'),
        'group_sale_ok': fields.function(get_group_sale_ok, type='boolean', method=True, string='group_sale_ok'),
        'num_dibujo': fields.char("Numero Dibujo"),
        'ruta_prod': fields.char("Ruta Produccion"),
    }

    def write(self, cr, uid, ids, vals, context=None):
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']               
        #usuario restriccion producto solo lectura
        dummy_product_readonly,group_id_product_readonly = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_product_readonly')
        #usuario restriccion en compras
        dummy_compra_rest,group_id_compra_rest = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_purchase_restriction')
        #usuarios de almacen
        #dummy_stock,group_id_stock = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'group_stock_user')
        #usuarios de contabilidad
        #dummy_account,group_id_account = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'group_account_user')
        #if group_id_stock not in user_group_ids or group_id_account not in user_group_ids:    
        if group_id_product_readonly in user_group_ids and not group_id_compra_rest in user_group_ids:
            raise osv.except_osv('Warning!',"Usted no tiene permiso para Modificar/Crear productos!")
        
        if group_id_compra_rest in user_group_ids:
            if vals.get('seller_ids') or vals.get('image_medium') or vals.get('image'): #contiene los proveedores:
                return super(product_product, self).write(cr, uid, ids, vals, context = context)
            else:
                raise osv.except_osv('Warning!',"Usted no tiene permiso para Modificar/Crear productos!")
        return super(product_product, self).write(cr, uid, ids, vals, context = context)
    
    def unlink(self, cr, uid, ids, context=None):
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']               
        #usuario restriccion producto solo lectura
        dummy_product_readonly,group_id_product_readonly = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_product_readonly')        
        if group_id_product_readonly in user_group_ids:
            raise osv.except_osv('Warning!',"Usted no tiene permiso para Eliminar productos!")
        
        return super(product_product, self).unlink(cr, uid, ids, context = context)
    
    def create(self, cr, uid, vals, context=None):
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']               
        #usuario restriccion producto solo lectura
        dummy_product_readonly,group_id_product_readonly = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_product_readonly')        
        if group_id_product_readonly in user_group_ids:
            raise osv.except_osv('Warning!',"Usted no tiene permiso para Crear productos!")
        return super(product_product, self).create(cr, uid, vals, context = context)
    
    def copy(self, cr, uid, id, default=None, context=None):
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']               
        #usuario restriccion producto solo lectura
        dummy_product_readonly,group_id_product_readonly = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields', 'ak_group_product_readonly')        
        if group_id_product_readonly in user_group_ids:
            raise osv.except_osv('Warning!',"Usted no tiene permiso para Duplicar productos!")            
        return super(product_product, self).copy(cr, uid, id, default=default, context = context)