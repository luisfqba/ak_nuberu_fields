# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields

class mrp_production(osv.Model):
    """
    Production Orders / Manufacturing Orders
    """
    _inherit = 'mrp.production'
    
    _columns = {
        'type_order': fields.selection((('L','Linea'),('E','Proyectos especiales'),('P','Prototipo'),('R','Retrabajo')), 'Tipo de Orden'),
        'num_dibujo': fields.related('product_id', 'num_dibujo', type='char', relation='product.product',
                                           string='Numero Dibujo', store=True),
        'ruta_prod': fields.related('product_id', 'ruta_prod', type='char', relation='product.product',
                                     string='Ruta Produccion', store=True),
    }

class mrp_bom(osv.Model):
    """
    BOM / Manufacturing Orders
    """
    _inherit = 'mrp.bom'
    
    _columns = {
        'code': fields.char('Reference', size=32),
    }
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
