# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP s.a. (<http://www.openerp.com>).
#    Copyright (C) 2012-TODAY Mentis d.o.o. (<http://www.mentis.si/openerp>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _
from collections import defaultdict
import datetime
import logging
_logger = logging.getLogger(__name__)

class purchase_order(osv.Model):
    _inherit = "purchase.order"
    """
    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        res = super(purchase_order, self).fields_view_get(cr, uid, view_id, view_type, context,
                                        toolbar, submenu)



        # default_type = context.get('default_type', False)
        # can_create = default_type != 'out_customer_production'
        update = view_type in ['form']

        if update:
            current_id = context.get('active_id', False)
            my_state = self.browse(cr, uid, current_id).state
            if my_state == 'new':
                pass


            from lxml import etree
            doc = etree.XML(res['arch'])
            doc.set('edit', 'false')
            doc.set('create', 'false')
            for node in doc.xpath("//button"):
                modifiers = '{"invisible": "1"}'
                node.set('modifiers', modifiers)
            res['arch'] = etree.tostring(doc)

        return res
    """
    def _clasificacion_prov_precio(self, cr, uid, fields, context=None):
        _logger.info("Start Do Async Process Precio")
        # fecha actual
        fecha = datetime.datetime.now()
        sem = 1
        mes1 = '>= 1'
        mes2 = '<= 6'
        if fecha.month > 6:
            sem = 2
            mes1 = '> 6'
            mes2 = '<= 12'

        # actualizar clasificacion de proveedor en productos
        query_sql_semestre1 = """select pol.product_id,pol.partner_id,date_part('year',po.date_approve),max(pol.price_unit)
                        from purchase_order_line pol
                        JOIN purchase_order po
                        ON pol.order_id = po.id
                        where date_part('months',po.date_approve) %s
                        AND date_part('months',po.date_approve) %s
                        AND po.state not in ('cancel','draf','sent')
                        group by pol.product_id,pol.partner_id,date_part('year',po.date_approve)
                        order by pol.product_id,max""" % (mes1, mes2)
        cr.execute(query_sql_semestre1)
        lines = cr.dictfetchall()
        romper = False
        # fecha actual
        fecha = datetime.datetime.now()
        for record in lines:
            product = record['product_id']
            partner = record['partner_id']
            year = str(record['date_part'])[:4]
            precio = record['max']
            # actualizar clasificacion
            product_obj_ids = self.pool.get('product.product').browse(cr, uid, product, context)
            # bandare actualizar si esTrue
            dict_clasf = []
            list_clasf = []
            if product_obj_ids.seller_ids:
                for seller in product_obj_ids.seller_ids:
                    f_actualizar_clasif = False
                    if seller.name.id == partner:
                        # si existe una clasificacion se actualiza
                        query_select_clasificacion = """select id, cal_precio, sem, year from clasificacion_proveedor_precio
                            where product_id = %s and partner_id = %s and sem = '%s' and year = '%s' limit 1""" % (
                            product_obj_ids.id, seller.name.id, sem, year)
                        cr.execute(query_select_clasificacion)
                        clasificacion_db = cr.dictfetchall()
                        # si este ya una clasificacion revisar si esta en el semestre actual
                        # y actualizar el precio si es necesario
                        if clasificacion_db:
                            clasificacion_db_id = clasificacion_db[0]['id']
                            clasificacion_db_precio = clasificacion_db[0]['cal_precio']
                            if clasificacion_db_precio != precio:
                                self.pool.get('clasificacion.proveedor.precio').write(cr, uid, clasificacion_db_id, {
                                    'cal_precio': precio, 'cal': 1,
                                })
                                f_actualizar_clasif = True
                        else:
                            # si no se crea
                            # si existe el proveedor se actualiza
                            clasificacio_new_id = self.pool.get('clasificacion.proveedor.precio').create(cr, uid, {
                                'cal_precio': precio,
                                'cal': 1,
                                'sem': sem,
                                'product_supplierinfo_id': seller.id,
                                'product_id': product_obj_ids.id,
                                'year': year,
                                'partner_id': seller.name.id,
                            })
                            f_actualizar_clasif = True
                    # clasificar por orden de mayor a menor precio
                    if f_actualizar_clasif:
                        for seller_update in product_obj_ids.seller_ids:
                            for x in seller_update.clasificacion_proveedor_precio_ids:
                                if fecha.year == int(x.year) and x.sem == sem:
                                    # no se agregan clasificaciones repetidas
                                    if not (x.id, x.cal_precio) in dict_clasf:
                                        dict_clasf.append((x.id, x.cal_precio))

                        groups = defaultdict(list)

                        for k in dict_clasf:
                            groups[k[1]].append(k[0])
                        l = groups.items()
                        l.sort(key=lambda x: x[0])
                        cont = 4
                        for item in l:
                            precio = item[0]
                            supplier_info = item[1]
                            cal = cont
                            # se actualiza la clasificacion
                            self.pool.get('clasificacion.proveedor.precio').write(cr, uid, supplier_info, {
                                'cal': cal,
                            })
                            cont = cont - 1
        _logger.info("End Do Async Process Precio")

    def _clasificacion_prov_sem2(self, cr, uid, fields, context=None):
        # actualizar clasificacion de proveedor en productos
        _logger.info("Start Do Async Process Calidad")

        query_sql_semestre2 = (
            "select pol.product_id,pol.partner_id,date_part('year',po.date_approve),max(pol.price_unit)\n"
            "            from purchase_order_line pol\n"
            "            JOIN purchase_order po \n"
            "            ON pol.order_id = po.id\n"
            "            where date_part('months',po.date_approve)> 6 \n"
            "            AND date_part('months',po.date_approve)<= 12\n"
            "            AND po.state not in ('cancel','draf','sent')\n"
            "            group by pol.product_id,pol.partner_id,date_part('year',po.date_approve)\n"
            "            order by pol.product_id,max")

        cr.execute(query_sql_semestre2)
        lines = cr.dictfetchall()
        romper = False
        # fecha actual
        fecha = datetime.datetime.now()
        for record in lines:
            product = record['product_id']
            partner = record['partner_id']
            year = str(record['date_part'])[:4]
            precio = record['max']

            # actualizar clasificacion
            product_obj_ids = self.pool.get('product.product').browse(cr, uid, product, context)

            dict_clasf = []
            list_clasf = []
            if product_obj_ids.seller_ids:
                for seller in product_obj_ids.seller_ids:
                    f_actualizar_clasif = False

                    if seller.name.id == partner:
                        query_select_clasificacion = """select id, cal_precio, sem, year from clasificacion_proveedor
                            where product_supplierinfo_id = %s and sem = '2' and year = '%s' limit 1""" % (
                            seller.id, year)
                        cr.execute(query_select_clasificacion)
                        clasificacion_db = cr.dictfetchall()
                        # si este ya una clasificacion revisar si esta en el semestre actual
                        ## y actualizar el precio si es necesario
                        if fecha.month > 6 and fecha.year == int(year):
                            if clasificacion_db:
                                clasificacion_db_id = clasificacion_db[0]['id']
                                clasificacion_db_precio = clasificacion_db[0]['cal_precio']
                                if clasificacion_db_precio != precio:
                                    self.pool.get('clasificacion.proveedor').write(cr, uid, clasificacion_db_id, {
                                        'cal_precio': precio, 'cal': 1,
                                    })
                                    f_actualizar_clasif = True
                            else:
                                # si no se crea
                                # si existe el proveedor se actualiza
                                clasificacio_new_id = self.pool.get('clasificacion.proveedor').create(cr, uid, {
                                    'cal_precio': precio,
                                    'cal': 1,
                                    'sem': 2,
                                    'product_supplierinfo_id': seller.id,
                                    'year': year,
                                    'partner_id': seller.name.id,
                                })
                                f_actualizar_clasif = True
                    # calificar por orden de mayor a menor precio
                    if f_actualizar_clasif:
                        # selecciono todas las calificaciones del producto
                        for seller_update in product_obj_ids.seller_ids:
                            for x in seller_update.clasificacion_proveedor_ids:
                                if fecha.year == int(x.year) and x.sem == 1:
                                    dict_clasf.append((x.id, x.cal_precio))
                        list_clasf = sorted(dict_clasf, key=lambda tup: tup[1])
                        cont = 4
                        for item in list_clasf:
                            key = item[0]
                            cal = cont
                            # se actualiza la clasificacion
                            self.pool.get('clasificacion.proveedor').write(cr, uid, key, {
                                'cal': cal,
                            })
                            cont = cont - 1

    def _clasificacion_prov_calidad(self, cr, uid, fields, context=None):
        _logger.info("Start Do Async Process Calidad")
        # fecha actual
        fecha = datetime.datetime.now()
        sem = 1
        mes1 = '>= 1'
        mes2 = '<= 6'
        if fecha.month > 6:
            sem = 2
            mes1 = '> 6'
            mes2 = '<= 12'

        # actualizar clasificacion de proveedor en productos
        query_sql_semestre1 = """select dev.product_id, dev.partner_id, cant_compras, cant_devoluciones,
            (100 / cant_compras * cant_devoluciones) AS porcentaje, dev.year
            from (
            select product_id, sm.partner_id, sum(product_qty) as cant_devoluciones, date_part('year', sm.date) as year
            from stock_move sm
            join stock_picking sp
            on sp.id = sm.picking_id
            and sp.type = 'out'
            and sp.purchase_id isnull = False
            and date_part('months',sp.date_done) %s
            and date_part('months',sp.date_done) %s
            and sp.state = 'done'
            and sm.return_type = '1'
            group by product_id, sm.partner_id, product_qty, year) dev
            join
            (
            select product_id, sm.partner_id, sum(product_qty)  as cant_compras, date_part('year', sm.date) as year
            from stock_move sm
            join stock_picking sp
            on sp.id = sm.picking_id
            and sp.type = 'in'
            and sp.purchase_id isnull = False
            and date_part('months',sp.date_done) %s
            and date_part('months',sp.date_done) %s
            and sp.state = 'done'
            group by product_id, sm.partner_id, year) comp
            on dev.product_id = comp.product_id
            and dev.partner_id = comp.partner_id""" % (mes1, mes2, mes1, mes2)

        cr.execute(query_sql_semestre1)
        lines = cr.dictfetchall()

        for record in lines:
            product = record['product_id']
            partner = record['partner_id']
            year = str(record['year'])[:4]
            porcentaje = record['porcentaje']

            if porcentaje > 5:
                cal = 1
            elif 2 < porcentaje <= 5:
                cal = 2
            elif 1 <= porcentaje <= 2:
                cal = 3

            # actualizar clasificacion
            product_obj_ids = self.pool.get('product.product').browse(cr, uid, product, context)

            if product_obj_ids.seller_ids:
                for seller in product_obj_ids.seller_ids:
                    if seller.name.id == partner:
                        # si existe una clasificacion se actualiza
                        query_select_clasificacion = """select id, cal, sem, year from clasificacion_proveedor_calidad
                            where product_id = %s and partner_id = %s and sem = '%s' and year = '%s' limit 1""" % (
                            product_obj_ids.id, seller.name.id, sem, year)

                        cr.execute(query_select_clasificacion)
                        clasificacion_db = cr.dictfetchall()
                        # si este ya una clasificacion revisar si esta en el semestre actual
                        # y actualizar el precio si es necesario
                        if clasificacion_db:
                            clasificacion_db_id = clasificacion_db[0]['id']
                            clasificacion_db_cal = clasificacion_db[0]['cal']
                            if clasificacion_db_cal != cal:
                                self.pool.get('clasificacion.proveedor.calidad').write(cr, uid, clasificacion_db_id, {
                                    'cal': cal,
                                })
                        else:
                            # si no se crea
                            # si existe el proveedor se actualiza
                            clasificacio_new_id = self.pool.get('clasificacion.proveedor.calidad').create(cr, uid, {
                                'sem': sem,
                                'product_supplierinfo_id': seller.id,
                                'year': year,
                                'partner_id': seller.name.id,
                                'product_id': product_obj_ids.id,
                                'cal': cal
                            })
        _logger.info("End Do Async Process Calidad")

    def _clasificacion_prov_tiempo(self, cr, uid, fields, context=None):
        _logger.info("Start Do Async Process Tiempo")

        fecha = datetime.datetime.now()
        sem = 1
        mes1 = '>= 1'
        mes2 = '<= 6'
        if fecha.month > 6:
            sem = 2
            mes1 = '> 6'
            mes2 = '<= 12'

        # actualizar clasificacion de proveedor en productos
        query_sql_semestre1 = """
        select sm.partner_id, date_part('year', sm.date) as year,
        AVG(date_part('days',sm.date) - date_part('days',sm.date_expected)) as promedio
        from stock_move sm
        join stock_picking sp
        on sp.id = sm.picking_id
        and sp.type = 'in'
        and sp.purchase_id isnull = False
        and date_part('months',sp.date_done) %s
        and date_part('months',sp.date_done) %s
        and sm.state = 'done'
        and sp.state = 'done'
        group by sm.partner_id, year""" % (mes1,mes2)
        cr.execute(query_sql_semestre1)
        lines = cr.dictfetchall()
        romper = False
        # fecha actual
        fecha = datetime.datetime.now()
        for record in lines:
            partner_id = record['partner_id']
            year = str(record['year'])[:4]
            promedio = record['promedio']
            cal = 4 # promedio < 1
            if promedio == 1:
                cal = 3
            elif promedio > 2:
                cal = 1

            # actualizar clasificacion
            #proveedores
            partner_obj= self.pool.get('res.partner').browse(cr, uid, partner_id, context)
            # bandare actualizar si esTrue
            dict_clasf = []
            list_clasf = []
            if partner_obj:
                #for partner_obj in partner_obj_ids:
                # si existe una clasificacion se actualiza
                query_select_clasificacion = """select id, cal, sem, year from clasificacion_proveedor_tiempo
                    where partner_id = %s and sem = '%s' and year = '%s' limit 1""" % (
                    partner_obj.id, sem, year)
                cr.execute(query_select_clasificacion)
                clasificacion_db = cr.dictfetchall()
                # si este ya una clasificacion revisar si existe clasificacion en el semestre actual
                # y actualizar el precio si es necesario
                #if fecha.month <= 6 and fecha.year == int(year):
                if clasificacion_db:
                    clasificacion_db_id = clasificacion_db[0]['id']
                    clasificacion_db_cal = clasificacion_db[0]['cal']
                    if clasificacion_db_cal != cal:
                        self.pool.get('clasificacion.proveedor.tiempo').write(cr, uid, clasificacion_db_id, {
                            'cal': cal,
                        })
                else:
                    # si no se crea
                    # si existe el proveedor se actualiza
                    clasificacio_new_id = self.pool.get('clasificacion.proveedor.tiempo').create(cr, uid, {
                        'sem': sem,
                        'year': year,
                        'partner_id': partner_obj.id,
                        'cal': cal
                    })
        _logger.info("End Do Async Process Tiempo")

    def _clasificacion_4_prov_calidad(self, cr, uid, fields, context=None):
        _logger.info("Start Do Async Process Calidad 4")

        # fehca actual
        fecha = datetime.datetime.now()
        # semestre y meses a comprar
        sem = 1
        mes1 = '>= 1'
        mes2 = '<= 6'
        if fecha.month > 6:
            sem = 2
            mes1 = '> 6'
            mes2 = '<= 12'

        # partner que no tienen devoluciones
        query_sql_semestre1 = """select sm.product_id, sm.partner_id, date_part('year', sm.date) as year
            from stock_move sm
            join stock_picking sp
            on sp.id = sm.picking_id
            and sp.type = 'in'
            and sp.purchase_id isnull = False
            and date_part('months',sp.date_done) %s
            and date_part('months',sp.date_done) %s
            and sp.state = 'done'
            where sm.partner_id not in
            (
            select distinct (sm.partner_id)
            from stock_move sm
            join stock_picking sp
            on sp.id = sm.picking_id
            and sp.type = 'out'
            and sp.purchase_id isnull = False
            and date_part('months',sp.date_done) %s
            and date_part('months',sp.date_done) %s
            and sp.state = 'done'
            and sm.return_type = '1'
            )
            group by sm.product_id, sm.partner_id, year""" % (mes1, mes2, mes1, mes2)
        cr.execute(query_sql_semestre1)
        lines = cr.dictfetchall()
        for record in lines:
            partner_id = record['partner_id']
            product_id = record['product_id']
            year = str(record['year'])[:4]

            # actualizar clasificacion
            product_obj_ids = self.pool.get('product.product').browse(cr, uid, product_id, context)
            # bandare actualizar si esTrue

            if product_obj_ids.seller_ids:
                for seller in product_obj_ids.seller_ids:
                    if seller.name.id == partner_id:
                        cal = 4
                        # si existe una clasificacion se actualiza
                        query_select_clasificacion = """select id, cal, sem, year from clasificacion_proveedor_calidad
                                where product_supplierinfo_id = %s and sem = '%s' and year = '%s' limit 1""" % (
                            seller.id, sem, year)
                        cr.execute(query_select_clasificacion)
                        clasificacion_db = cr.dictfetchall()
                        # si este ya una clasificacion revisar si esta en el semestre actual
                        # y actualizar el precio si es necesario
                        #if fecha.month <= 6 and fecha.year == int(year):
                        if clasificacion_db:
                            clasificacion_db_id = clasificacion_db[0]['id']
                            clasificacion_db_cal = clasificacion_db[0]['cal']
                            if clasificacion_db_cal != cal:
                                self.pool.get('clasificacion.proveedor.calidad').write(cr, uid, clasificacion_db_id, {
                                    'cal': cal,
                                })
                        else:
                            # si no se crea
                            # si existe el proveedor se actualiza
                            clasificacio_new_id = self.pool.get('clasificacion.proveedor.calidad').create(cr, uid, {
                                'sem': sem,
                                'product_supplierinfo_id': seller.id,
                                'year': year,
                                'partner_id': seller.name.id,
                                'product_id': product_obj_ids.id,
                                'cal': cal
                            })
        _logger.info("End Do Async Process Calidad 4")

    def _set_eval_precio(self, cr, uid, fields, context=None):

        fecha = datetime.datetime.now()
        semestre = 1
        if fecha.month > 6:
            semestre = 2
        year = fecha.year
        # se obtiene el id del proveedor y su calificacion de precio
        query_sql_eval = (
                          "select partner_id, ROUND(AVG(cal)::NUMERIC,0) as cal \n"
                          "from clasificacion_proveedor_precio \n"
                          "where sem=%s and year='%s' \n"
                          "GROUP BY partner_id"
                          ) % (semestre, str(year))
        cr.execute(query_sql_eval)
        lines = cr.dictfetchall()

        # actualiza proveedores con calificacion
        supplier_obj = self.pool.get('res.partner')
        for line in lines:
            supplier_obj.write(
                cr, uid,
                line['partner_id'],
                {'eval_precio':line['cal']},
                context=context
            )

    def _set_eval_calidad(self, cr, uid, fields, context=None):

        fecha = datetime.datetime.now()
        semestre = 1
        if fecha.month > 6:
            semestre = 2
        year = fecha.year
        # se obtiene el id del proveedor y su calificacion de precio
        query_sql_eval = (
                             "select partner_id, ROUND(AVG(cal)::NUMERIC,0) as cal \n"
                             "from clasificacion_proveedor_calidad \n"
                             "where sem=%s and year='%s' \n"
                             "GROUP BY partner_id"
                         ) % (semestre, str(year))
        cr.execute(query_sql_eval)
        lines = cr.dictfetchall()

        # actualiza proveedores con calificacion
        supplier_obj = self.pool.get('res.partner')
        for line in lines:
            supplier_obj.write(
                cr, uid,
                line['partner_id'],
                {'eval_calidad_producto': line['cal']},
                context=context
            )

    def _set_eval_tiempo(self, cr, uid, fields, context=None):

        fecha = datetime.datetime.now()
        semestre = 1
        if fecha.month > 6:
            semestre = 2
        year = fecha.year
        # se obtiene el id del proveedor y su calificacion de precio
        query_sql_eval = (
                             "select partner_id, cal \n"
                             "from clasificacion_proveedor_tiempo \n"
                             "where sem=%s and year='%s' \n"
                             "GROUP BY partner_id, cal"
                         ) % (semestre, str(year))
        cr.execute(query_sql_eval)
        lines = cr.dictfetchall()

        # actualiza proveedores con calificacion
        supplier_obj = self.pool.get('res.partner')
        for line in lines:
            supplier_obj.write(
                cr, uid,
                line['partner_id'],
                {'eval_tiempo_entrega': line['cal']},
                context=context
            )

    def get_date_approve_picking_in(self, cr, uid, ids, name, args, context=None):
        """Funcion para agregar fecha de aprovacion de recepciones en tree pedidos de compra"""
        res = {}
        if not ids: return res
        result = ""
        for record in self.browse(cr, uid, ids, context=context):
            for pickin_in in record.picking_ids:
                if pickin_in.type == 'in' and pickin_in.state == 'done':
                    result = result + str(pickin_in.name) + ": " + str(pickin_in.date_done) + "\n"
            res[record.id] = result
            result = ""
        return res

    def get_group_purchase_approve(self, cr, uid, ids, name, args, context=None):
        """Funcion devuelve true si el usuario pertenece al grupo que puede aprobar compras"""
        res = {}
        if not ids: return res
        result = False
        dummy, group_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ak_nuberu_fields',
                                                                              'ak_group_purchase_approve')
        user_group_ids = self.pool.get('res.users').read(cr, uid, [uid], context)[0]['groups_id']
        for record in self.browse(cr, uid, ids, context=context):
            if group_id in user_group_ids:
                result = True
                res[record.id] = result
                return res
        res[record.id] = result
        return res

    _columns = {
        'date_approve_picking_in': fields.function(get_date_approve_picking_in, type='char', method=False,
                                                   string='Fecha Recepcion Almacen'),
        'group_purchase_approve': fields.function(get_group_purchase_approve, type='boolean', method=True,
                                             string='can_purchase_approve'),
    }

    def _set_proveedor_indicador(self, cr, uid, ids=None, context=None, supplier_id = None, indicador=None, evals=None):
        _logger.info("Start Do Async Process Indicador Proveedor")

        fecha = datetime.datetime.now()
        sem = 1  # semestre 1
        if fecha.month > 6:
            sem = 2  # semestre 2
        year = fecha.year

        # si existe una indicador
        query_select_supplier_indicador = """select id, sem, year, eval_precio, eval_tiempo_entrega,
                        eval_calidad_producto, eval_condiciones_pago, eval_servicio from supplier_indicador
                        where partner_id = %s and sem = '%s' and year = '%s' limit 1""" % (
            supplier_id, sem, year)
        cr.execute(query_select_supplier_indicador)
        indicadores_db = cr.dictfetchall()
        # si ya existe un indicador revisar si esta en el semestre actual
        # y actualizar el indicador si es necesario
        # if fecha.month <= 6 and fecha.year == int(year):
        if indicadores_db:
            indicador_id = indicadores_db[0]['id']
            eval_precio = float(indicadores_db[0]['eval_precio'])
            eval_tiempo_entrega = float(indicadores_db[0]['eval_tiempo_entrega'])
            eval_calidad_producto = float(indicadores_db[0]['eval_calidad_producto'])
            eval_condiciones_pago = float( indicadores_db[0]['eval_condiciones_pago'])
            eval_servicio = int(indicadores_db[0]['eval_servicio'])
            indicador_avg = (eval_precio + eval_tiempo_entrega + eval_calidad_producto + eval_condiciones_pago + eval_servicio)/5.00
            #indicador = sum(evals)/5.00
            if indicador_avg != indicador:
                self.pool.get('supplier.indicador').write(cr, uid, indicador_id, {
                    'eval_precio': eval_precio,
                    'eval_tiempo_entrega': eval_tiempo_entrega,
                    'eval_calidad_producto': eval_calidad_producto,
                    'eval_condiciones_pago': eval_condiciones_pago,
                    'eval_servicio': eval_servicio
                })
        else:
            # si no se tiene registro de indicador del proveedor en el semestre se crea
            clasificacio_new_id = self.pool.get('supplier.indicador').create(cr, uid, {
                'sem': sem,
                'partner_id': supplier_id,
                'year': year,
                'eval_precio': evals['eval_precio'],
                'eval_tiempo_entrega': evals['eval_tiempo_entrega'],
                'eval_calidad_producto': evals['eval_calidad_producto'],
                'eval_condiciones_pago': evals['eval_condiciones_pago'],
                'eval_servicio': evals['eval_servicio']
            })
        _logger.info("End Do Async Process Indicador proveedor")


    def _cal_proveedor_indicador(self, cr, uid, ids=None, context=None):
        # actualiza proveedores con el indicador del día
        supplier_obj = self.pool.get('res.partner')
        supplier_ids=supplier_obj.search(cr, uid, [('supplier', '=', True), ('active', '=', True)])
        cant = len(supplier_ids)
        suppliers = self.pool.get('res.partner').browse(cr, uid, supplier_ids, context=context)
        for record in suppliers:
            eval_precio = float(record.eval_precio)
            eval_tiempo_entrega = float(record.eval_tiempo_entrega)
            eval_calidad_producto = float(record.eval_calidad_producto)
            eval_condiciones_pago = float(record.eval_condiciones_pago)
            eval_servicio = float(record.eval_servicio)

            indicador = (eval_precio + eval_tiempo_entrega + eval_calidad_producto + eval_condiciones_pago + eval_servicio) / 5.00
            supplier_obj.write(
                cr, uid,
                record.id,
                {'eval_promedio': indicador},# revisar si es mejor actualizar el indicador al final del dia
                context=context
            )
            evals = {
                    'eval_precio': eval_precio,
                    'eval_tiempo_entrega': eval_tiempo_entrega,
                    'eval_calidad_producto': eval_calidad_producto,
                    'eval_condiciones_pago': eval_condiciones_pago,
                    'eval_servicio': eval_servicio
                }

            self._set_proveedor_indicador(cr, uid, ids, context, record.id, indicador, evals)

    def do_async_evaluacion_prov(self, cr, uid, ids=None, context=None):
        ''' Run Async process.
        @param ids
        '''
        if context is None:
            context = {}

        _logger.info("Start Do Async Process")
        # calcular clasificaciones
        self._clasificacion_prov_precio(cr, uid, fields, context=None)
        self._clasificacion_prov_calidad(cr, uid, fields, context=None)
        self._clasificacion_prov_tiempo(cr, uid, fields, context=None)
        # actualizar a 4 los que no tienen devoluciones
        self._clasificacion_4_prov_calidad(cr, uid, fields, context=None)

        # actualizar proveedores
        self._set_eval_precio(cr, uid, fields, context=None)
        self._set_eval_tiempo(cr, uid, fields, context=None)
        self._set_eval_calidad(cr, uid, fields, context=None)
        self._cal_proveedor_indicador(cr, uid, ids=None, context=None)


        _logger.info("Finish Do Async Process")

        return []


class purchase_order_line(osv.Model):
    _inherit = "purchase.order.line"
    _columns = {
        'product_order_id': fields.many2one('mrp.production', 'Orden de Produccion'),
    }


class purchase_requisition_line(osv.Model):
    _inherit = "purchase.requisition.line"
    _columns = {
        'product_order_id': fields.many2one('mrp.production', 'Orden de Produccion'),
    }


class purchase_requisition(osv.Model):
    _inherit = "purchase.requisition"

    _columns = {
        'create_date': fields.datetime('Fecha de Creacion', readonly=True),
    }

    """
     Se sobreescribe el metodo para pasar la orden de trabajo(product_order_id) desde
     la requisision a la orden de compra.  
    """

    def make_purchase_order(self, cr, uid, ids, partner_id, context=None):
        """
        Create New RFQ for Supplier
        """
        if context is None:
            context = {}
        assert partner_id, 'Supplier should be specified'
        purchase_order = self.pool.get('purchase.order')
        purchase_order_line = self.pool.get('purchase.order.line')
        res_partner = self.pool.get('res.partner')
        fiscal_position = self.pool.get('account.fiscal.position')
        supplier = res_partner.browse(cr, uid, partner_id, context=context)
        supplier_pricelist = supplier.property_product_pricelist_purchase or False
        res = {}
        for requisition in self.browse(cr, uid, ids, context=context):
            if supplier.id in filter(lambda x: x, [rfq.state <> 'cancel' and rfq.partner_id.id or None for rfq in
                                                   requisition.purchase_ids]):
                raise osv.except_osv(_('Warning!'), _(
                    'You have already one %s purchase order for this partner, you must cancel this purchase order to create a new quotation.') % rfq.state)
            location_id = requisition.warehouse_id.lot_input_id.id
            purchase_id = purchase_order.create(cr, uid, {
                'origin': requisition.name,
                'partner_id': supplier.id,
                'pricelist_id': supplier_pricelist.id,
                'location_id': location_id,
                'company_id': requisition.company_id.id,
                'fiscal_position': supplier.property_account_position and supplier.property_account_position.id or False,
                'requisition_id': requisition.id,
                'notes': requisition.description,
                'warehouse_id': requisition.warehouse_id.id,
            })
            res[requisition.id] = purchase_id
            for line in requisition.line_ids:
                product = line.product_id
                seller_price, qty, default_uom_po_id, date_planned = self._seller_details(cr, uid, line, supplier,
                                                                                          context=context)
                taxes_ids = product.supplier_taxes_id
                taxes = fiscal_position.map_tax(cr, uid, supplier.property_account_position, taxes_ids)
                purchase_order_line.create(cr, uid, {
                    'order_id': purchase_id,
                    'name': product.partner_ref,
                    'product_qty': qty,
                    'product_id': product.id,
                    'product_uom': default_uom_po_id,
                    'price_unit': seller_price,
                    'date_planned': date_planned,
                        'taxes_id': [(6, 0, taxes)],
                    'product_order_id': line.product_order_id.id,  # orden de produccion proveniente de la requisicion
                }, context=context)

        return res


class clasificacion_proveedor_precio(osv.Model):
    _name = "clasificacion.proveedor.precio"
    _columns = {
        'cal_precio': fields.float(string='Clasif Precio'),
        'cal': fields.integer(string='Clasificion'),
        'sem': fields.integer(string='Semestre'),
        'year': fields.char(string='Año'),
        'partner_id': fields.many2one('res.partner', 'Proveedor'),
        'product_id': fields.many2one('product.product', 'Producto'),
        'product_supplierinfo_id': fields.many2one('product.supplierinfo', 'Abastecimiento Proveedor'),
    }

class clasificacion_proveedor_calidad(osv.Model):
    _name = "clasificacion.proveedor.calidad"
    _columns = {
        'cal': fields.integer(string='Clasificion'),
        'sem': fields.integer(string='Semestre'),
        'year': fields.char(string='Año'),
        'partner_id': fields.many2one('res.partner', 'Proveedor'),
        'product_id': fields.many2one('product.product', 'Producto'),
        'product_supplierinfo_id': fields.many2one('product.supplierinfo', 'Abastecimiento Proveedor'),
    }

class product_supplierinfo(osv.Model):
    _inherit = "product.supplierinfo"
    _columns = {
        'clasificacion_proveedor_precio_ids': fields.one2many('clasificacion.proveedor.precio', 'product_supplierinfo_id',
                                                       string='Clasificacion Precio'),
        'clasificacion_proveedor_calidad_ids': fields.one2many('clasificacion.proveedor.calidad', 'product_supplierinfo_id',
                                                       string='Clasificacion Calidad'),
    }

class clasificacion_proveedor_tiempo(osv.Model):
    _name = "clasificacion.proveedor.tiempo"
    _columns = {
        'cal': fields.integer(string='Clasificion'),
        'sem': fields.integer(string='Semestre'),
        'year': fields.char(string='Año'),
        'partner_id': fields.many2one('res.partner', 'Proveedor'),
    }

class res_partner(osv.Model):
    _inherit = "res.partner"
    _columns = {
        'clasificacion_proveedor_tiempo_ids': fields.one2many('clasificacion.proveedor.tiempo', 'partner_id',
                                                       string='Clasificacion Tiempo Entrega'),
        'supplier_indicador_ids': fields.one2many('supplier.indicador', 'partner_id',
                                                              string='Indicador Proveedor'),
    }

class supplier_indicador(osv.Model):
    """Esta tabla almacena el indicador del proveedor por semestre"""
    _name = "supplier.indicador"

    def _calc_avg_indicador(self, cr, uid, ids, name, args, context=None):
        res = {}
        if not ids: return res
        result = 0.0
        for record in self.browse(cr, uid, ids, context=context):
            result = float(record.eval_precio) + float(record.eval_tiempo_entrega) + float(
                record.eval_calidad_producto) + float(record.eval_condiciones_pago) + float(record.eval_servicio)
            res[record.id] = result / 5.0
        return res

    _columns = {
        'indicador': fields.function(_calc_avg_indicador, type='float', method=True, string='Indicador'),
        'sem': fields.integer(string='Semestre'),
        'year': fields.char(string='Año'),
        'partner_id': fields.many2one('res.partner', 'Proveedor'),
        'eval_precio': fields.integer('Precio'),
        'eval_tiempo_entrega': fields.integer('Tiempo Entrega'),
        'eval_calidad_producto': fields.integer('Calidad'),
        'eval_condiciones_pago': fields.integer('Condiciones Pago'),
        'eval_servicio': fields.integer(string='Servicio'),
    }
